#!/bin/bash

source /home/airnet/echan_play/channel.cfg

if [ "$ECH_INTERNET_TYPE" = "ethernet" ]; then
        /home/airnet/echan_play/launch_echannel.sh
elif [ "$ECH_INTERNET_TYPE" = "wifi" ]; then
        /home/airnet/echan_play/wifi.sh
elif [ "$ECH_INTERNET_TYPE" = "mobile" ]; then
	/home/airnet/echan_play/mobile.sh
fi;

