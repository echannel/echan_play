#!/bin/bash

source /home/airnet/echan_play/channel.cfg

if [ "$ECH_BROWSER" == "firefox"  ]; then
	firefox http://195.67.148.130/player.html?player=${ECH_PLAYER_ID}
elif [ "$ECH_BROWSER" == "chrome"  ]; then
	google-chrome --start-fullscreen --restore-last-session-state --unlimited-storage http://195.67.148.130/player.html?player=${ECH_PLAYER_ID}
fi;
