#!/bin/bash

FIRST=1
CONNECTED=0
xscreensaver -no-splash &
while true; do
    #LC_ALL=C nmcli -t -f TYPE,STATE dev | grep -q "^gsm:disconnected$"
		LC_ALL=C nmcli -t -f TYPE,STATE dev | grep "gsm:\(unavailable\|disconnected\)"
    if [ $? -eq 0 ]; then
        nmcli -t nm wwan off
        sleep 1
        nmcli -t nm wwan on
        sleep 1
        nmcli -t con up id "Telia Telia 3G"
        sleep 15
        CONNECTED=0
				echo "Not connected!"
    else
				echo "Connected!"
        CONNECTED=1
    fi
    sleep 2

    ping -q -w1 -c1 8.8.8.8 
		if [ $? -eq 0 ] && [ "$FIRST" = 1 ]; then
			echo "Launching echannel"
			FIRST=0
			/home/airnet/echan_play/launch_echannel.sh
		fi  
done

