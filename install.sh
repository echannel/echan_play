whoami | grep airnet
if [ $? -ne "0" ]; then
	echo "Run the script as airnet!"
	exit
fi

software_install=0
ssh_install=0
chrome_install=0
tv_install=0
autostart_install=0

while getopts "0sctawh" opt; do
	case "$opt" in
	0)
		software_install=1
		ssh_install=1
		chrome_install=1
		tv_install=1
		autostart_install=1
		;;
	s)
		ssh_install=1
		;;
	c)
		chrome_install=1
		;;
	t)
		tv_install=1
		;;
	a)
		autostart_install=1
		;;
	w)
		software_install=1
		;;
	esac
done

if [ $chrome_install -eq 1 ]; then
	echo "Installing chrome"
	wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	sudo dpkg -i google-chrome*.deb
	rm google-chrome*.deb
fi

if [ $tv_install -eq 1 ]; then
	#echo "Installing teamviewer"
	wget http://www.teamviewer.com/download/teamviewer_linux.deb
	sudo dpkg -i teamviewer_linux.deb
	sudo apt-get install -f -y
	rm teamviewer_linux.deb

	mkdir -p ~/.config/autostart
fi

if [ $software_install -eq 1 ]; then
	sudo apt-get update

	echo "Installing git and openssh-server"
	sudo apt-get install -f -y git openssh-server seahorse x11vnc vinagre firefox ubuntu-restricted-addons ubuntu-restricted-extras unclutter xdotool

	echo "Installing screensaver"
	sudo apt-get install -f -y xscreensaver-data-extra xscreensaver-gl-extra xscreensaver
	gsettings set org.cinnamon.desktop.lockdown disable-lock-screen true
fi

if [ $ssh_install -eq 1  ]; then
	echo "Configure ssh"
	ssh-keygen -q -t rsa -f ~/.ssh/id_rsa -N ""
fi

if [ $autostart_install -eq 1  ]; then
	echo "Configuring autostart..."
	mkdir -p ~/.config/autostart
	ln -s /home/airnet/echan_play/autossh.desktop /home/airnet/echan_play/xscreensaver.desktop /home/airnet/echan_play/xtras.desktop /home/airnet/echan_play/close_tv_popup.sh /home/airnet/echan_play/internet.desktop /home/airnet/echan_play/unclutter.desktop ~/.config/autostart/ 
fi

