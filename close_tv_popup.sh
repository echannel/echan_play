#!/bin/bash

export DISPLAY=:0

while true 
do
	while read line
	do
		echo $line
		xdotool windowactivate --sync $line key --clearmodifiers Tab
		xdotool windowactivate --sync $line key --clearmodifiers Shift+Tab
		xdotool windowactivate --sync $line key --clearmodifiers Return
	done <<< "`xdotool search Sponsored 2> /dev/null`"

	sleep 30s
done
